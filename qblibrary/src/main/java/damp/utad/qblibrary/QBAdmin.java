package damp.utad.qblibrary;

import android.os.Bundle;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;

import java.util.ArrayList;
import java.util.List;


public class QBAdmin {
/*
Esta es la clase  donde se encuentran los datos de nuestra BBDD de QB



 */

    private ArrayList<QB_Listener> listener2=new ArrayList<QB_Listener>();

   QB_Listener listener = null;

    public QB_Listener getListener() {
        return listener;
    }

    public void setListener(QB_Listener listener) {
        this.listener =  listener;
    }


   public QBAdmin(){
       QBSettings.getInstance().fastConfigInit("33806", "WExx4MNu87U-O3m", "MHTjYuacY5gwuE9");
    }
    public void setQbAdminListener(QB_Listener listener2){
        this.listener2.add(listener2);
    }
    public void removeQBAdminListener(QB_Listener listener2){
        this.listener2.remove(listener2);
    }


    public void crearSession(){
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                // success

                listener.crear_sesion(true);

            }

            @Override
            public void onError(List<String> errors) {
                // errors

                listener.crear_sesion(false);

            }
        });
    }

    }


