package damp.utad.qblibrary;

import android.os.Bundle;

import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.List;

public class QBUsers_Login {


    /*
    Esta es la clase que nos permitira loguearnos


     */
    boolean conectado;
    private QBUsersListener listener;

    public void addQBUserLoginListener(QBUsersListener list) {
        listener = list;
    }


    public void loginUsuario(String usuario, String password) {

        QBUser user = new QBUser(usuario, password);

        QBUsers.signIn(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle params) {
                conectado = true;
                listener.login(conectado);
            }

            @Override
            public void onError(List<String> errors) {
                conectado = false;
                listener.login(conectado);
            }
        });
    }
}
