package damp.utad.examen_final_3_jose_mansilla;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by germy on 05/03/2016.
 */
public class Registro extends AppCompatActivity {
    EditText username;
    EditText inputPassword;
    Button rg ;
    Button rg2 ;
    Login log;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);
/*
relacion y setonclicklistener de los botones del layout registro
 */

rg =(Button) findViewById(R.id.btnregister);
        rg.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                registro();


            }
        });
        rg2 =(Button) findViewById(R.id.button_volver2);
        rg2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

              inimenu();

            }
        });
    }



    /*
    Metodo que permite el registro
     */
    public void registro(){

        username = (EditText) findViewById(R.id.txtnmb);
        inputPassword = (EditText) findViewById(R.id.txtpass2);
        String j =username.getText().toString();
        final QBUser user = new QBUser(j,inputPassword.getText().toString());
/*
muestra un toast cuando se registra el usuario
 */

        QBUsers.signUp(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                Log.v("Registrado2", " Registrado ");
                Toast.makeText(Registro.this, " registrado ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Log.v("Registrado2", " Registrado " );
                Toast.makeText(Registro.this, " registrado ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(List<String> list) {

            }


        });

}
    //intent del boton volver, vuelve al login
    public void inimenu(){
        Intent i = new Intent(this, Login.class);
        startActivity(i);
    }


}
