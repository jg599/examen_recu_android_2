package damp.utad.examen_final_3_jose_mansilla;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import damp.utad.qblibrary.QBAdmin;
import damp.utad.qblibrary.QBUsersListener;
import damp.utad.qblibrary.QBUsers_Login;
import damp.utad.qblibrary.QB_Listener;


public class Login extends AppCompatActivity implements QB_Listener, QBUsersListener {
//declaro las variables
    private TextView lblGotoRegister;
    private Button btnLogin;
    private Button btnRegistro;
    private EditText username;
    private EditText inputPassword;

    private String nombre;
    private String password;
    private CheckBox box_recordar;
    QBAdmin qbAdmin;
    QBUsers_Login qbuser;

    Registro r;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

//relacion entre las variables y el layout
        username = (EditText) findViewById(R.id.nombre);
        inputPassword = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistro = (Button) findViewById(R.id.btnregistro);
        box_recordar = (CheckBox) findViewById(R.id.box_recordar);
        r =new Registro();
        qbAdmin = new QBAdmin();

        qbuser = new QBUsers_Login();

        qbAdmin.setListener(this);

        qbuser.addQBUserLoginListener(this);
/*
setOnClickListener de boton login y registro
 */

        qbAdmin.setListener(this);
        qbAdmin.crearSession();
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                 nombre = username.getText().toString();
                 password = inputPassword.getText().toString();
                logear(nombre, password);

            }
        });
        btnRegistro.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
            abrir_registro();
           //    registro();

            }
        });


    }


    @Override
    public void crear_sesion(boolean check) {
        /*
        obtenemos el shared preferences
         */
        SharedPreferences preferencias = getSharedPreferences("Almacenamiento_maps", 0);
       nombre = preferencias.getString("user", null);
        password  = preferencias.getString("pass", null);
        Log.v("sessionCreada", " session creada " + nombre);
        Log.v("sessionCreada", " session creada " + password);



        if (check == true) {

            if (nombre != null && password != null) {
                logear(nombre, password);
            }
            Log.v("sessionCreada", " session creada " + check);

        } else {
            Log.v("sessionCreada", " session NO creada " + check);
        }
    }
/*
Añado la condicion if en el metodo loguear para que funcione correctamente el shared preferences
 */

    public void logear(String nombre, String password) {
if(nombre ==null) {
    nombre = String.valueOf(username.getText());
    password = String.valueOf(inputPassword.getText());
}
        qbuser.loginUsuario(nombre, password);
    }

    @Override
    public void login(boolean logeado) {
        /*
        implementando shared preferences
         */
        SharedPreferences settings = getSharedPreferences("Almacenamiento_maps", 0);
        if (logeado == true) {
//cuando se marque el checbox se guardara el usuario y la contraseña
            if (box_recordar.isChecked()) {
                SharedPreferences.Editor editor=settings.edit();
                editor.putString("user", username.getText().toString());
                editor.putString("pass", inputPassword.getText().toString());
                editor.commit();

            }

            abrir_mapa();

//etiqueta de aviso de login
            Toast.makeText(Login.this, username.getText() + " Ha logueado con éxito ", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(Login.this, " Usuario o contraseña inválidos ", Toast.LENGTH_SHORT).show();
        }

    }


//intent para abrir el mapa
    public void abrir_mapa() {
       Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }
    //intent para abrir registro
    public void abrir_registro() {
        Intent i = new Intent(this, Registro.class);
        startActivity(i);
    }


}
